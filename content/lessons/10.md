---
title: "Nets"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "10"
puzzles:
- id: pzl1
  target: 10-1
  text: Puzzle 1
- id: pzl2
  target: 10-2
  text: Puzzle 2
returnTo: "lessons/11"
section: "index.html#techniques"
---

# | Nets
## First ladders, now nets. We have a lot of weird metaphors.

> Sometimes (or rather often) capturing something means not attacking directly but rather, surrounding a bit from a distance.

**If you place your stones too close to your opponent's, they may become a target and while you are busy rescuing your stones, your opponent gets away.**

However, if you play just the right distance away, your opponent will still not have enough space to make life (create 2 eyes) and you will have enough time to safely connect all your stones. Sounds simple? Test it out! 

{{< tsumego >}}

That's all there is to it. Netting something just means surrounding from a distance and waiting patiently. Go is all about balance. Do not be too greedy or passive. Do not attack too aggressively but also do not let your opponent get away without any pressure. Always try for just a tiny bit more than what your opponent is getting. 