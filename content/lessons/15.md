---
title: "Endgame (yose)"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "15"
puzzles:
- id: pzl1
  target: 15-1
  text: Endgame
- id: pzl2
  target: 15-2
  text: Endgame tricks
returnTo: "lessons/16"
section: "index.html#strategies"
---

# | Is Endgame (Yose) Important?
## Yes, it is.

> For some reason the endgame is often an overlooked part of the game of Go. I am not sure why. People often think the game is decided before it comes to endgame, and sometimes it is, but if you have chosen an evenly-matched opponent, endgame should be the final showdown where everything is decided. 

**It is not even hard to understand. There are some tricks of course but overall it is usually just about diligence. With a good endgame you can gain as much as 10 points fairly easily.**

Once all the big points are taken, the board is divided between the two players, and all the big fights have ended and nothing big is likely to die, we enter endgame. To play a decent endgame follow these rules:

{{< rule >}}
	Sente moves {{< black "first!" >}}
{{< /rule >}}
If there is unlikely to be any big ko fights be sure to take EVERY <a data-hover="(Moves you expect your opponent to answer directly, thus keeping the initiative to chose where you will play next)" class="sgfLink">sente move</a> you can. Even if it is only 1 point and your opponent has to answer, it is one point for you and nothing for your opponent. Find 10 moves like this and you will have earned 10 points.

{{< rule >}}
	Do {{< black "not" >}} answer every attack blindly.
{{< /rule >}}
During endgame, havin sente means making points. Consider sacrificing even a few points (by not answering an attack) to gain sente. It is fine to lose 3 points of territory if you can make 5 points just by gaining sente. If you are only answering you are not gaining any points.

{{< rule >}}
	Once you are out of sente moves, chose the {{< black "biggest" >}} gote play.
{{< /rule >}} 
If you have to give up sente, you better make it as profitable as possible.

That's it. It is all fairly logical and easy (in theory). Yet I see games every day, where players fail to finish a won game, just because of a complete lack of endgame understanding.

{{< tsumego >}}

See how big the difference can be even on the 9x9 board? On 19x19 it would be even bigger.
 

