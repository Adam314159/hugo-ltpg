---
title: "Your first Go Reward"
date: 2019-03-31T10:05:17+02:00
draft: false
layout: "info"
returnTo: "index.html#techniques"
---

# | Being a Go Player
## What your first actual game will be like...

> In 2000 Cho Chikun (one of Japan's most famous Go players) lost his most prized title (25th Meijin) to Yoda Morimoto 4-nil. He was asked by a reporter, "Why do you like Go so much?".  "I hate Go.", came the famous reply.

If you want to play Go, you have to be ready to lose. Chances are you will lose your first game. Chances are you will lose the second and the third... It might be 50 games before you also start winning on some regular basis. Right now, you have no idea what gulf there is between you and barely mediocre players. The learning curve is **VERY** LONG. But you WILL start winning games. You WILL improve. The day will come. Go will teach you to appreciate the journey over the destination... or break you.

When you finally start getting noticeably better you will still lose LOTS of games. It will stay like that for as long as you play. No matter what you learn, no matter how much your reading improves, there will always be players even better than you. Always. Forever. That is why we hate this game, but it is also why we love it. To death. If you can't take that, leave while you still can. If you can live with that, then welcome among us. Come over to {{< externalLink target="https://online-go.com/register" name="OGS" >}} and if he is still there, tell {{< externalLink target="https://online-go.com/player/360861" name="AdamR" >}} that you finished our guide. Maybe even challenge him to your first game.

My advice to you is not to make winning the ultimate goal. Make the goal to improve instead or even better, to have fun. When presented with a choice between a safe winning move and not so safe, but fun looking one, remember this guide and choose the fun-move, as a thank you to us for taking the time to write this. Stop caring about the wins and they will come on their own. "Every good game has two winners", *anonymous*.

To set the mood, here is a short animation that I made ages ago. Enjoy...and when you are done watching, play some games of 9x9 Go. After that, maybe try some of the advanced topics.

{{< youtube quqTe1KfmHQ >}}

{{< alert "mt-8" >}} 
And if you want to get some perspective about how hard the go journey will be, check out this awesome {{< externalLink target="https://www.deviantart.com/inkwolf/gallery/74369340/aji-s-quest" name="comic" >}}
{{< /alert >}} 	