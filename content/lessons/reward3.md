---
title: "Your third Go Reward"
date: 2019-03-31T10:05:17+02:00
draft: false
layout: info
returnTo: "index.html#what_now"
---

# | Mistakes, mistakes everywhere
## And I mean everywhere...

>  "Play every move as if it were the first in your life" - Sekiyama Riichi


**And now you are on your own.**

Be free. And thank you for reading this far. Never expected anyone to read so much of my rambling. And now I have nothing more to say. You will have to discover the rest on your own, or with the help of someone else. Check out some video lectures or look for strong players to help you make smaller mistakes. 

Yes, when you think about it a move can never be good in Go. It can only NOT be a mistake when you are lucky (or that good). But don't ever feel bad about bad moves! Even pro players do them, so you can too.

Have fun!

{{< youtube qt1FvPxmmfE >}}

