---
title: "Placing the Stones"
date: 2019-03-31T10:07:35+02:00
draft: false
cookieSetting: "s1"
returnTo: "index.html#rules"
batch: "2"
puzzles:
- id: pzl1
  target: 2/solutions/0
  text: Kill White
- id: pzl2
  target: 2/solutions/1
  text: Save Black
- id: pzl3
  target: 2/solutions/2
  text: Connect
- id: pzl4
  target: 2/solutions/3
  text: Capture
- id: pzl5
  target: 2/solutions/4
  text: Snapback
---

# | Solutions to quiz #2
## It’s fine to celebrate success but it is more important to heed the lessons of failure.<br><br> 

Here, the puzzles you have just tried are explained. The ones you have failed are highlighted red.

{{< tsumego show="true" >}}

{{< quizDisplay >}}