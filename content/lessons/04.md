---
title: "Ko"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "4"
returnTo: "lessons/05"
section: "index.html#rules"
puzzles:
- id: pzl1
  target: 4-1
  text: Puzzle 1
- id: pzl2
  target: 4-2
  text: Puzzle 2
- id: pzl3
  target: 4-3
  text: Puzzle 3  
---

# | No Repetition (A rule called "Ko")
## No repetition ever.

> Full disclosure - this is a rule that may confuse you at first. Fortunately, it is not as complicated as it appears at first glance. I for one think that a good ko fight is one of the most fun things you can have in Go, although many would disagree.

**A game of Go can be really long. However, it must never be endless. To prevent the possibility of an endless game we have a rule called "Ko".**

Just imagine a situation like this one. Given enough stubbornness, players like these would be playing until they pass out. Feel free to watch for a while.

![ko diagram](/images/ko.gif)

To prevent health problems connected with exhaustion, we have the following rule:

{{< rule >}}
    A completely identical board position {{< black "must not">}} appear twice in one game.
{{< /rule >}}

{{< rule >}}
Therefore, before you can recapture a stone that just captured yours, you have to {{< black "play elsewhere">}} at least {{< black "once">}}.
{{< /rule >}}

**Can you ever capture that stone then? What are your options? Basically, you have two:**

⬤  You deem the stone not important for now and play elsewhere, letting your opponent end the ko by connecting her endangered stone if she wants.

⬤  You deem the stone important enough to fight the ko. You still have to play elsewhere, but you try to find a move your opponent will feel compelled to answer to immediately instead of resolving the original ko; we call this a ko-threat. After that, you retake the ko stone and the fight is on!



*Confusing? Check out the following examples. It will be clear in no time.*

{{< tsumego >}}

On the 19x19 board, there are usually much more than only one threat, so the ko fight can span several moves.
The hard part, of course, is judging if a given threat is big enough that it needs to be responded to or whether it is more beneficial to end the ko. But hey, I said the rules are easy, not the game itself.