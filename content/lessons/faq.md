---
title: "Placing the Stones"
date: 2019-03-31T10:07:35+02:00
draft: false
cookieSetting: "1"
returnTo: "index.html#rules"
section: "index.html#rules"
---

# | FAQ
## Frequently asked questions about Go rules and other stuff

> Some questions come up repeatedly (and quite naturally) when one is learning the game. If you are not sure about some situation, chances are you will find your answer below. You can link to any particular answer just by opening it up and then sharing the URL.

**Questions about rules**

{{< extra "random-stones" "Can't I just throw a random stone into my opponent's territory,  force him to capture it, and make him/her lose 4 points?" >}}
	As you probably gathered, it does not work like that otherwise everyone would be doing it. <br><br>
	As discussed in <a href="/lessons/05"><u>chapter 5</u></a>, a <b>stone that cannot survive is counted as a prisoner anyway</b> (even if it remains on the board). Unless the stone is threatening some real harm or capture, your opponent can in fact just ignore the move and get an extra point for the prisoner you just gave him. If you then want to argue the stone is alive, the game position may be played out, but the score is then counted from the original position after the two passes, not the played out version.
{{< /extra >}}

{{< extra "seki" "If groups are at a sort of an impasse and neither side can capture the other, how is it scored?" >}}
	You are probably talking about "seki", in which case neither player gets nor loses any points for those groups (under Japanese scoring). It is further discussed in <a href="/lessons/12"><u>chapter 12</u>.</a>
{{< /extra >}}

{{< extra "rulesets" "There are more rulesets in Go? What are the differences?" >}}
	There are, but it does not really matter most of the time. The result is usually the same no matter what rules you played under.
	If you are super curious, check out the basic differences <a href="/lessons/rulesets"><u>here</u></a>.
{{< /extra >}}

{{< extra "bent4" "What is the bent four rule?" >}}
	It is a rule stating that a group in a certain position is dead (under Japanese scoring) even though it may not look like it at first glance. It causes a lot of confusion that comes from not exploring the position and a confusion of terms. It is best explained on a board.<br><br>
	Check out the example situation <a href="https://online-go.com/demo/view/414006" target="_blank" noreferrer noopener><u>here</u></a>.
{{< /extra >}}

{{< extra "chinese-easier" "Is it true that under Chinese rules I can play inside my own territory or let my stones die without losing points (since captures are not worth anything)?" >}}
	Yes and no (mostly not). This is a common misconception and a pet peeve of mine. The above statement is true <b>ONLY</b> under very specific conditions. When all stones are safe, all borders finished and every last neutral point (dame) taken, then (<b>AND ONLY THEN</b>) yes. But at this point it is usually too late to change anything (it makes scoring disputes much easier though). 
	<br><br>Until the game arrives to this point playing inside your own territory or losing a stone <b>costs you a point under both rulesets.</b> While Japanese rules specify that these actions are -1 point for you, under Chinese rules, you just do not get the extra point that you get for any alive stones elsewhere on the board because both territory and the surrounding stones count for points. The result is the same; you are one point short.  
	
{{< /extra >}}

<br>

**Questions about history and traditions**

{{< extra "ranks" "What is kyu and dan / How do the traditional ranks work?" >}}
	As a tradition we use an old Japanese ranking system.
	<br><br>
	<b>Kyu</b> (k for short) - is a "student's" rank. It starts at a high number (most commonly 25k, but it can differ) and as a student grows in strength, the number decreases - 1k being the strongest kyu rank.
	<br><br>
	<b>Dan</b> (d for short) - is a "master's" rank. It starts at a low number (1d being the lowest dan rank - following immediately after 1k) and 6d usually being the strongest dan level. Sometimes these are called amateur dan to differentiate from professional dan levels.
	<br><br>
	<b>Professional dan</b> (p for short) - is a "master's" rank that is recognized by some of the professional organizations. In other words, playing Go is their job. They are usually even stronger than amateur dans, but the skill level can vary. Pro ranks start at 1p and the strongest obtainable level (not counting honorary titles) is 9p.
{{< /extra >}}

{{< extra "oldest-game" "I have heard that Go is the oldest board game in the world. Is it true?" >}}
	These claims are rather hard to verify, but there is probably some truth to them. As far as we know, Go is the oldest board game still being regularly played today (in more or less its original form).
{{< /extra >}}

{{< extra "go-vs-computers" "What is all the fuss about computers and Go?" >}}
	For the longest time, Go was the only remaining game in which computers were unable to beat top human players (and not for the lack of trying).<br><br>
	This changed with the advent of neural networks and a program called AlphaGo (by Google) that managed to beat one of the very best players at the time (Lee Sedol) 4-1 in March 2016. Since then the strength of Go programs has been unsurpassed by humans. 
{{< /extra >}}

{{< extra "polite-move" "A polite first move" >}}
	It is traditional to play the first move in the upper right corner (from your perspective) of the board. This way a player symbolically bows to their opponent (White is traditionally the stronger player) and makes the most expected second move (upper left corner) easy to play. This habit is still common, even in online play.	
{{< /extra >}}

<br>

**Questions about learning**

{{< extra "learning-with-bots" "Is it okay to start by playing bots?" >}}
	If you first need to gain some confidence before playing real humans, it is of course fine to test yourslef against a bot in a game or two. Most Go players, however, agree that it is not a good idea to play them for long. Most servers are full of complete beginners just like you, and many have strong players willing to help you out. Just play them. It is much more exciting, much more fun, and a much better learning experience as they can also give you feedback.
{{< /extra >}}

{{< extra "switching-to-19x19" "When should I switch to 19x19 board?" >}}
	My personal answer is whenever you want to. You can switch right now if you like, but there is no rush! It is important to realize that a lot changes on the big board. It is a much longer game, and there are many more possible moves to think about. If you are still having trouble with some of the basic concpets (like recognizing atari), it does not make much sense to introduce new troubles for yourself and spend 30 minutes on a game you are not yet completely sure how to play.
{{< /extra >}}

{{< extra "reaching-dan" "How long does it take to reach dan levels?" >}}
	You probably do not want to hear this, but many players never reach dan level. For others it varies a lot.<br>
	But to give you a general idea, it would be quite impressive (but not unheard of) if you reached dan level within a year.
{{< /extra >}}
