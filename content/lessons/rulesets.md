---
title: "Rulesets"
date: 2019-03-31T10:07:35+02:00
draft: false
layout: info
cookieSetting: "1"
returnTo: "lessons/05#bottom"
section: "index.html#rules"
---

# | Different rulesets
## Different and yet the same <br><br>

> There are several rule systems being used in Go. Sometimes it can make a difference, but most of the time (given reasonable play) the winner will remain constant regardless of what rules are used.

Here, I will list the most important differences. There are some more technical details that are also not constant across rulesets (like ending a game on superko) which are not relevant for our level of games just yet.

**Japanese rules**

⬤ Only surrounded empty intersections count for points, not the stones surrounding them<br> 
⬤ Captured stones are worth 1 point each for their captor<br>
⬤ Suicide is not allowed<br>
⬤ Komi is 6.5 points on 19x19<br>

*These are the rules I have taught you*

**Chinese rules (and for our purposes also AGA)**

⬤ Both surrounded empty intersections and the stones surrounding them count for points<br> 
⬤ Captured stones are worth nothing<br>
⬤ Suicide is not allowed<br>
⬤ Komi is 7.5 points on 19x19<br>

*That does not mean that losing a stone is a bigger loss in Japanese rules than in Chinese! While it is a common misconceptions among beginners, if your stone gets captured, it is exactly the same loss in both rulesets. In Japanese your opponent gets an extra point for capture and in Chinese you do not get that 1 extra point you get for every alive stone. It is +1 -1 and the result is the same. It can only make a difference once ALL territories are defined and EVERY neutral point is taken. Then losing another stone or playing inside your territory no longer matters under Chinese rules while it still does in Japanese*

**New Zealand rules**

⬤ Both surrounded empty intersections and the stones surrounding them count for points<br> 
⬤ Captured stones are worth nothing<br>
⬤ Suicide is allowed<br>
⬤ Komi is 7 points on 19x19 (Game can be a draw)<br>

**ING rules**

⬤ Komi is 8 points on 19x19 (But Black wins in case of a tie)<br>
⬤ Different ko rules, which we will not get into here (see {{< externalLink target="https://senseis.xmp.net/?IngKoRule" name="Sensei's library" >}} if you really want to know).

*Otherwise same as New Zeland