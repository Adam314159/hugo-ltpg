---
title: "Quizz on basic rules"
date: 2019-03-31T10:07:35+02:00
draft: false
cookieSetting: "q1"
returnTo: "index.html#rules"
batch: "1"
nrOfQurestions: "4"
---

# | Quiz on basic rules
## If you want to make sure you have a firm grasp on everything <br><br>

The rules may have seemed easy in isolated examples, but it takes a bit of practice to apply them to real (and sometimes complicated) games. You only have one chance for each question.

{{< quizz illegal="true" >}}

If you want to further examine some of the quiz puzzles, check out the solutions <a href="../../lessons/solutions1" noreferrer noopener><u>here</u></a>. 
